read -p "Masukkan username: " username

if [ ! -d log.txt ]; then
    touch log.txt
fi

if [ ! -d ./users ]; then
    install -d ./users
fi

if [ ! -f ./users/user.txt ]; then
    install -D /dev/null ./users/users.txt
fi
if grep -q "^$username:" ./users/users.txt; then
  printf "User sudah exists.\n"
  printf "$(date +"%y/%m/%d %T") REGISTER: ERROR User already exists\n" >> log.txt
  exit 1
fi

read -s -p "Masukkan password: " password
echo


if [[ ${#password} < 8 ]] || ! [[ "$password" =~ [[:lower:]] && "$password" =~ [[:upper:]]&& "$password" =~ [[:digit:]] && "$password" =~ ^[[:alnum:]]+$ ]] || [[ "$password" == "$username" ]] || [[ "$password" =~ chicken|ernie ]]; then
  printf "Password minimal 8 karakter, memiliki minimal 1 huruf kapital dan 1 huruf kecil,alphanumeric, tidak boleh sama dengan username ,tidak boleh menggunakan kata chicken atau ernie.\n"
  printf "$(date +"%y/%m/%d %T") REGISTER: ERROR Password is not secure for user $username\n" >> log.txt
  exit 1
fi

printf "%s:%s\n" "$username" "$password" >> ./users/users.txt

printf "User %s sudah registrasi.\n" "$username"
printf "$(date +"%y/%m/%d %T") REGISTER: INFO User $username registered successfully\n" >> log.txt

exit 0