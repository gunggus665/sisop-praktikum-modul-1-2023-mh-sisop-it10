read -p "Masukkan username: " username

if ! grep -q "^$username:" ./users/users.txt; then
  printf "User tidak exist.\n"
  printf "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username\n" >> log.txt
  exit 1
fi

read -s -p "Masukkan password: " password
echo

saved_password=$(grep "^$username:" ./users/users.txt | cut -d':' -f2)

if [[ "$password" != "$saved_password" ]]; then
  printf "Password salah.\n"
  printf "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username\n" >> log.txt
  exit 1
fi

printf "Login berhasil\n"
printf "$(date +'%y/%m/%d %T') LOGIN: INFO User %s logged in\n" "$username" >> log.txt