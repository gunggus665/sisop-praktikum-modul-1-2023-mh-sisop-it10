#!/bin/bash
currentTime=$(date +%H:%M)
currentDate=$(date +%d:%m:%Y)
filename="${currentTime} ${currentDate}.txt"

inputFile="/var/log/syslog"
outputFile="${filename}"

echo "Sedang Mengenkripsi"


#otomatis menambahkan cronjob untuk menjalankan program ini tiap 2 jam ke crontab
#0 */2 * * * $PWD/log_encrypt.sh pwd adalah directory program ini berada
if crontab -l 2>/dev/null | grep -q "$PWD/log_encrypt.sh"; then
    (crontab -l 2>/dev/null | grep -v "$PWD/log_encrypt.sh" ; echo "0 */2 * * * $PWD/log_encrypt.sh") | crontab - >/dev/null 2>&1
else
    (crontab -l 2>/dev/null ; echo "0 */2 * * * $PWD/log_encrypt.sh") | crontab - >/dev/null 2>&1
fi

currentHour=$(date +'%H' | sed 's/^0//')
while read line
do
    output=""
    for ((i=0; i<${#line}; i++)); do
        char=${line:$i:1}
        isUp=0
        if [[ "$char" =~ [a-zA-Z] ]]; then
            desChar=$(printf '%d' "'$char")
            if [[ $desChar -ge 65 && $desChar -le 90 ]]; then
                isUp=1
            fi
            if (( $desChar + currentHour > 90 && $isUp )) || (( $desChar + currentHour > 122 && ! $isUp )); then
                charEnkrip=$(printf \\$(printf '%03o' "$(( $desChar + currentHour - 26 ))"))
                output="${output}${charEnkrip}"
            else
                charEnkrip=$(printf \\$(printf '%03o' "$(( $desChar + currentHour ))"))
                output="${output}${charEnkrip}"
            fi
        else
            output="${output}${char}"
        fi
    done

    echo "${output}" >> "${outputFile}"
done < "${inputFile}"
echo "Selesai"
