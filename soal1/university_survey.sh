#!/bin/sh

file="2023 QS World University Rankings.csv"
japanHighRank=$(cat "$file" | tr -d '"' | awk '/JP/ {print}' | sort -t ',' -k 1n | head -n 5 )
japanLowFSR=$(cat "$file" | tr -d '"' | awk '/JP/ {print}' | sort -t ',' -k 9n | head -n 1)
japanHighGer=$(cat "$file" | tr -d '"' | awk '/JP/ {print}' | sort -t ',' -k 20n | head -n 10)
kampusKeren=$(cat "$file" | tr -d '"' | grep Keren)
echo "========Japan Highest Rank========"
echo "$japanHighRank"
echo ""
echo "========Japan Lowest FSR========"
echo "$japanLowFSR"  
echo ""
echo "========Japan Highest Ger Rank========"
echo "$japanHighGer"    
echo ""
echo "=====KAMPUS KEREN NIH KAK====="
echo "$kampusKeren"
