## Soal 1

## Analisa soal :
Untuk pengerjaan soal nomer satu diminta untuk mengurutkan beberapa data yang sudah diberikan pada file .csv hal yang pertama kali dilakukan adalah mencari command atau perintah pada ubuntu untuk bisa mensort serta mengambil data yang diinginkan. Setelah mengetahui command apa yang digunakan selanjutnya langsung mengikuti perintah pada soal.

## Cara pengerjaan soal 1 : 
Setelah melakukan analisa soal pertama tama saya melakukan deklarasi variable yang nantinya akan digunakan untuk menyelesaikan perintah pada soal selanjutnya saya mengikuti perintah berdasarkan soal contoh pada poin A diminta untuk  menampilkan 5 Universitas dengan ranking tertinggi di Jepang, hal yang saya lakukan adalah mencari kolom berdasarkan ranknya terlebih dahulu kemudian menggunakan perintah awk untuk mengambil columnnya lalu melakukan sort dari atas kebawah, untuk soal 2 dan 3 cara yang dilakukan kurang lebih sama namun kolomnya saaja yang berbeda. Namun untuk yang terakhir saya menggunakan grep untuk mengambil universitas yang memiliki kata Keren

## Source code :  
    
```bash
#!/bin/sh

file="2023 QS World University Rankings.csv"
japanHighRank=$(cat "$file" | tr -d '"' | awk '/JP/ {print}' | sort -t ',' -k 1n | head -n 5 )
japanLowFSR=$(cat "$file" | tr -d '"' | awk '/JP/ {print}' | sort -t ',' -k 9n | head -n 5)
japanHighGer=$(cat "$file" | tr -d '"' | awk '/JP/ {print}' | sort -t ',' -k 20n | head -n 10)
kampusKeren=$(cat "$file" | tr -d '"' | grep Keren)
echo "========Japan Highest Rank========"
echo "$japanHighRank"
echo ""
echo "========Japan Lowest FSR========"
echo "$japanLowFSR"  
echo ""
echo "========Japan Highest Ger Rank========"
echo "$japanHighGer"    
echo ""
echo "=====KAMPUS KEREN NIH KAK====="
echo "$kampusKeren"
```


## Test Output : 
```
========Japan Highest Rank========

23,The University of Tokyo,JP,Japan,100,7,99.7,9,91.9,78,73.3,128,10.4,601+,27.8,448,89.5,174,97.8,33,85.3

36,Kyoto University,JP,Japan,98.6,21,98.9,15,94.8,62,54.2,234,14.9,601+,22.1,503,85.5,233,56.9,201,81.4

55,Tokyo Institute of Technology (Tokyo Tech),JP,Japan,74.1,80,93.4,42,81.5,126,65.9,168,36.1,433,37.9,362,55.8,601+,33.6,377,72.5

68,Osaka University,JP,Japan,80.2,68,85.4,61,67.4,205,59.1,206,25,525,14.4,601+,75.2,385,21,567,68.2

79,Tohoku University,JP,Japan,71.8,86,78.1,75,98.6,43,34.2,401,14.1,601+,16.4,595,66.8,493,18.7,601+,64.9

========Japan Lowest FSR========

891,Ritsumeikan Asia Pacific University,JP,Japan,3.9,501+,10.4,501+,2.5,601+,2.7,601+,99.4,83,99.9,25,5.7,601+,8.1,601+,-

1277,Shibaura Institute of Technology,JP,Japan,3.1,501+,2.6,501+,2.6,601+,7,601+,22,558,9.1,601+,17.2,601+,4.1,601+,-

1244,Kwansei Gakuin University,JP,Japan,3.3,501+,7.4,501+,4.2,601+,3.2,601+,29.7,479,4.1,601+,17,601+,8.6,601+,-

1042,Doshisha University,JP,Japan,8,501+,15.7,501+,5.8,601+,2.1,601+,20.8,573,4.3,601+,12.3,601+,14.8,601+,-

1249,Meiji University,JP,Japan,6.4,501+,22.1,429,5.8,601+,1.8,601+,10.1,601+,6.2,601+,15.7,601+,16.5,601+,-

========Japan Highest Ger Rank========

23,The University of Tokyo,JP,Japan,100,7,99.7,9,91.9,78,73.3,128,10.4,601+,27.8,448,89.5,174,97.8,33,85.3

197,Keio University,JP,Japan,50.3,154,91.9,48,60.3,258,6.6,601+,10.6,601+,12.4,601+,46,601+,83.7,90,44.1

206,Waseda University,JP,Japan,58.7,121,96.5,27,27.2,558,4.4,601+,28.4,494,35.2,390,63.5,525,72.2,127,42.8

533,Hitotsubashi University,JP,Japan,13.9,501+,84.3,63,30.7,504,2.4,601+,21.2,565,21.9,507,6.4,601+,64.5,169,-911,Tokyo University of Science,JP,Japan,10.8,501+,46.2,206,11.2,601+,14.5,601+,12.5,601+,3.8,601+,27.7,601+,60.8,188,-

36,Kyoto University,JP,Japan,98.6,21,98.9,15,94.8,62,54.2,234,14.9,601+,22.1,503,85.5,233,56.9,201,81.4

112,Nagoya University,JP,Japan,59.8,117,54.3,153,90.6,84,33.9,403,15.8,601+,20,536,66.5,499,35.1,367,56.3

55,Tokyo Institute of Technology (Tokyo Tech),JP,Japan,74.1,80,93.4,42,81.5,126,65.9,168,36.1,433,37.9,362,55.8,601+,33.6,377,72.5

843,International Christian University,JP,Japan,4.3,501+,12,501+,16.6,601+,1.7,601+,76.4,231,14.8,601+,7,601+,33.5,379,-

135,Kyushu University,JP,Japan,56.6,133,65,113,84.9,110,26.8,466,15.1,601+,20.7,530,66.1,502,22.4,543,53.5

=====KAMPUS KEREN NIH KAK=====

711,Institut Teknologi Sepuluh Nopember (ITS Surabaya Keren),ID,Indonesia,13,501+,30.3,322,30.7,503,1.8,601+,60,307,2.9,601+,12.2,601+,19.8,595,
```

## Kendala : 
pada awalnya saya sedikit bingung kapan harus menggunakan awk dan grep kemudian setelah membaca pengertian saya sekarang ketika kita ingin mengambil spesifik file kita bisa menggunakan grep kalau awk kita ingin mengambil atau menselect perkolom



## Soal 2

## Analisa soal :
Pada soal nomer 2 diminta untuk mendownload file yang berupa gambar dengan format file perjalanan_nomor kemudian di simpan pada folder yang bernama folder_nomor file yang didownload berdasarkan jam sekarang misal sekarang jam 7 pagi maka foto akan terdownload sebanyak 7 kali foto dan gambarnya didownload 10 jam sekali kemudian juga akan di zip selama 1 hari 

## Cara pengerjaan soal 2 : 
Setelah melakukan analisa soal hal yang pertama dilakukan adalah mencari foto dan menyalin url untuk mendapatkan link downladnya, kemudian mengecek kondisi dengan findMe yang berisikan pengecekan apabila belum terdapat file yang mengandung prefix kumpulan dan dimulai dari 1 maka akan dibuatkan filenya dan berlaku untuk penambahan digit seterusnya. Kemudian mendklarasikan waktu saat ini dengan curr_time yang hanya mengambil jamnya saja karena yang dibutuhkan hanya jamnya saja. Kemudian membuat if condition untuk mengecek dan mendownload file pertama dan ditempatkan di path yang sudah dituju serta menyertakan link downloadnya. Untuk menyelesaikan poin satu diperlukan cron untuk meng-set interval waktu 10 jam dengan : crontab -l ; echo 0 */10 * * * dan melakukan if kondisi lagi untuk mendownload file hingga batas waktu saat itu dan untuk interval waktu zip menggunakan code : crontab -l; echo "0 0 * * * dan command zip {nama filezip} {file yang mau di zip}

## Source code : 
```bash
#!/bin/bash

curr_time=$(date +"%-H")
if [ curr_time = 0 ]; then
curr_time=1
fi
findMe=$(find /home/ggs -maxdepth 1 -type d -name "kumpulan_*" | grep -c "kumpulan_")
findMe=$((findMe+1))
foldername="kumpulan_$findMe"

mkdir "./kumpulan_$findMe"

file_url="https://www.touropia.com/gfx/b/2019/08/indonesia.png"
if [ $(ls -1 /home/ggs/$foldername | wc -l) -lt $curr_time ]; then wget -O /home/ggs/$foldername/perjalanan_$(( $(ls -1 /home/ggs/$foldername | wc -l) + 1 )).png $file_url; fi
(crontab -l ; echo "0 */10 * * * if [ \$(ls -1 /home/ggs/$foldername | wc -l) -lt $curr_time ]; then wget -O /home/ggs/$foldername/perjalanan_\$(( \$(ls -1 /home/ggs/$foldername | wc -l) + 1 )).png $file_url; fi") | crontab -
(crontab -l; echo "0 0 * * * for file in /home/ggs/*; do zip -j devil_\${file#/home/ggs/kumpulan_} \$file/*; done") | crontab -

```
## Test Output : 
```
ggs@LAPTOP-V2J920E7:~$ l

'2023 QS World University Rankings.csv'*   devil_1.zip   kobeni_liburan.sh   kumpulan_1/   university_survey.sh

ggs@LAPTOP-V2J920E7:~$ l kumpulan_1/

perjalanan_1.png  

ggs@LAPTOP-V2J920E7:~$ unzip devil_1.zip

Archive:  devil_1.zip

  inflating: perjalanan_1.png

```
## Kendala : 
pada awalnya saya sedikit bingung untuk membuat cron dan script dalam satu file, namun setelah membaca saya mulai mengerti bagaimana caranya dan juga sempat mengalami kendala pada saat ngezip awalnya isi zipnya berisi direktori /home/ggs/file namun akhirnya bisa terselesaikan dengan command zip -j

## Soal 3 

## Analisa soal :
Untuk nomer tiga, diinstruksikan untuk membuat sistem register dan login yang diletakkan pada file louis.sh dan retep.sh. Pada sistem tersebut, data login dan register harus tersimpan pada file log.txt  dengan format : YY/MM/DD hh:mm:ss MESSAGE dan data username yang masuk tercatat pada file users.txt dalam folder users. Dalam sistem register terdapat beberapa persyaratan yang harus dipenuhi antara lain yaitu minimal 8 karakter, memiliki minimal 1 huruf kapital dan 1 huruf kecil, alphanumeric, tidak boleh sama dengan username, dan tidak boleh menggunakan kata chicken atau ernie.Ketika mencoba register dengan username yang sudah terdaftar, maka pesan pada log adalah REGISTER: ERROR User already exists.Saat register berhasil,pesan pada log adalah REGISTER: INFO User USERNAME registered successfully.Saat user mencoba login namun passwordnya salah, maka pesan pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME.Saat user berhasil login, maka pesan pada log adalah LOGIN: INFO User USERNAME logged in.

# Cara pengerjaan soal 
Buat file retep.sh dan louis.sh menggunakan nano file.sh. Masukkan script bash yang sudah di sistem untuk menjalankan sistem register pada louis.sh dan sistem login pada retep.sh. Setelah itu run progam dengan bash file.sh. Masukkan username dan password dengan berbagai keadaan yang tidak sesuai dengan instruksi soal untuk mengecek apakah progam sudah terjalan dengan benar atau tidak. Apabila sistem sudah menampilkan output yang sesuai apabila sistem tidak benar, maka masukkan username dan password yang benar agar data register dan login tercatat pada log.txt dan users.txt. Setelah itu, cek pada file log.txt dan users.txt apakah sudah ada data yang dimaksudkan. 

# A. louis.sh
```bash
read -p "Masukkan username: " username

if [ ! -d log.txt ]; then
    touch log.txt
fi

if [ ! -d ./users ]; then
    install -d ./users
fi

if [ ! -f ./users/user.txt ]; then
    install -D /dev/null ./users/users.txt
fi
if grep -q "^$username:" ./users/users.txt; then
  printf "User sudah exists.\n"
  printf "$(date +"%y/%m/%d %T") REGISTER: ERROR User already exists\n" >> log.txt
  exit 1
fi

read -s -p "Masukkan password: " password
echo


if [[ ${#password} < 8 ]] || ! [[ "$password" =~ [[:lower:]] && "$password" =~ [[:upper:]]&& "$password" =~ [[:digit:]] && "$password" =~ ^[[:alnum:]]+$ ]] || [[ "$password" == "$username" ]] || [[ "$password" =~ chicken|ernie ]]; then
  printf "Password minimal 8 karakter, memiliki minimal 1 huruf kapital dan 1 huruf kecil,alphanumeric, tidak boleh sama dengan username ,tidak boleh menggunakan kata chicken atau ernie.\n"
  printf "$(date +"%y/%m/%d %T") REGISTER: ERROR Password is not secure for user $username\n" >> log.txt
  exit 1
fi

printf "%s:%s\n" "$username" "$password" >> ./users/users.txt

printf "User %s sudah registrasi.\n" "$username"
printf "$(date +"%y/%m/%d %T") REGISTER: INFO User $username registered successfully\n" >> log.txt

exit 0
```

1. **`read -p "Masukkan username: " username`**: untuk input username dan output yang keluar "Masukkan username: " dan menyimpan nilai
2. **`if [ ! -d log.txt ]; then touch log.txt; fi`**: Memeriksa file log.txt sudah ada apa belum. Jika tidak ada, maka file tersebut akan dibuat.
3. **`if [ ! -d ./users ]; then install -d ./users; fi`**: Memeriksa apakah folder users udah ada. Jika tidak ada, maka folder tersebut akan dibuat 
4. **`if [ ! -f ./users/user.txt ]; then install -D /dev/null ./users/users.txt; fi`**: Memeriksa apakah file users.txt sudah ada di dalam folder users. Jika tidak, maka file tersebut akan dibuat. 
5. **`if grep -q "^$username:" ./users/users.txt; then`**: Memeriksa apakah username yang dimasukkan sudah ada di dalam file users.txt
6. **`printf "User sudah exists.\n"`**: Menampilkan pesan error bahwa user sudah terdaftar.
7. **`printf "$(date +"%y/%m/%d %T") REGISTER: ERROR User already exists\n" >> log.txt`**: Mencatat log bahwa user sudah terdaftar pada file log.txt
8. **`read -s -p "Masukkan password: " password`**: Meminta pengguna untuk memasukkan password dengan menampilkan prompt "Masukkan password: " dan menyimpan nilai 
9. **`if [[ ${#password} < 8 ]] || ! [[ "$password" =~ [[:lower:]] && "$password" =~ [[:upper:]]&& "$password" =~ [[:digit:]] && "$password" =~ ^[[:alnum:]]+$ ]] || [[ "$password" == "$username" ]] || [[ "$password" =~ chicken|ernie ]]; then`**: Memeriksa apakah password yang dimasukkan sudah memenuhi kriteria yang diinginkan.
10. **`printf "$(date +"%y/%m/%d %T") REGISTER: ERROR Password is not secure for user $username\n" >> log.txt`**: Mencatat log bahwa password tidak aman pada file log.txt.
11. **`printf "%s:%s\n" "$username" "$password" >> ./users/users.txt`**: Menambahkan username dan password yang sudah dimasukkan oleh pengguna ke dalam file users.txt
12. **`printf "$(date +"%y/%m/%d %T") REGISTER: INFO User $username registered successfully\n" >> log.txt`** digunakan untuk mencatat log bahwa user baru berhasil terdaftar. 

# B. Retep.sh
```bash
read -p "Masukkan username: " username

if ! grep -q "^$username:" ./users/users.txt; then
  printf "User tidak exist.\n"
  printf "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username\n" >> log.txt
  exit 1
fi

read -s -p "Masukkan password: " password
echo

saved_password=$(grep "^$username:" ./users/users.txt | cut -d':' -f2)

if [[ "$password" != "$saved_password" ]]; then
  printf "Password salah.\n"
  printf "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username\n" >> log.txt
  exit 1
fi

printf "Login berhasil\n"
printf "$(date +'%y/%m/%d %T') LOGIN: INFO User %s logged in\n" "$username" >> log.txt
```
1. **`read -p "Masukkan username: " username`**: Untuk meminta input dari pengguna untuk memasukkan username.
2. **`if ! grep -q "^$username:" ./users/users.txt; then`**: Untuk memeriksa apakah username yang dimasukkan oleh pengguna sudah terdaftar atau belum. 
3. **`read -s -p "Masukkan password: " password`**: Perintah untuk meminta input dari pengguna untuk memasukkan password 
4. **`saved_password=$(grep "^$username:" ./users/users.txt | cut -d':' -f2)`**: Untuk mengambil password yang sudah tersimpan pada file users.txt berdasarkan username yang dimasukkan oleh pengguna. 
5. **`if [[ "$password" != "$saved_password" ]]; then`**: Perintah untuk membandingkan password yang dimasukkan oleh pengguna dengan password yang sudah tersimpan pada file users.txt. 
6. **`printf "Login berhasil\n"`**: Perintah untuk menampilkan pesan "Login berhasil" jika username dan password yang dimasukkan oleh pengguna sudah benar.
7. **`printf "$(date +'%y/%m/%d %T') LOGIN: INFO User %s logged in\n" "$username" >> log.txt`**: Perintah untuk mencatat log bahwa pengguna sudah berhasil login pada file log.txt.

# Test Output 


![O1](https://i.ibb.co/vQf4JhT/o1.png)

Letak file seperti directory berikut 

![O1](https://i.ibb.co/C5dMBMZ/02.png)

Apabila saat register password tidak masuk kriteria minimal 8 karakter, memiliki minimal 1 huruf kapital dan 1 huruf kecil,alphanumeric, tidak boleh sama dengan username ,tidak boleh menggunakan kata chicken atau ernie maka akan tampil output seperti berikut 

![O1](https://i.ibb.co/LxGRYm1/o3.png)

Apabila saat register password telah memenuhi syarat maka akan tampil output sebagai berikut

![O1](https://i.ibb.co/DQz2r1y/o4.png)

Apabila saat login password salah maka akan tampil output sebagai berikut

![O1](https://i.ibb.co/gRyCGrX/o5.png)

Apabila saat login username tidak terdaftar maka akan tampil output sebagai berikut

![O1](https://i.ibb.co/zGGf8Bv/o6.png)

Apabila saat login username dan password sudah sesuai maka akan menampilkan output berikut 

![O1](https://i.ibb.co/7QXY85p/o8.png)

![O1](https://i.ibb.co/KXBj4Ly/o9.png)

Pada file log.txt dan users.txt pada folder users saakan menampilkan data berikut 

# kendala

saya mencoba beberapa kali merubah script bash karena sistem register atau login tidak berjalan dengan sesuai sampai sistem nya berhasil

## Soal 4

## Analisa soal:
Diminta backup syslog ke sebuah file dengan format  jam:menit tanggal:bulan:tahun.txt. Dalam backup nya diminta untuk melakukan enkripsi dengan Caesar Cipher (Shift) dengan key nya adalah jam ketika program dijalankan. Kemudian diminta program backup dijalankan setiap 2 jam. Diminta juga membuat program untuk mendekripsi file backup tersebut.

## Cara pengerjaan soal:
Mengambil isi teks di file syslog, kemudian masukkan per line ke file baru sesuai format soal, sebelum dimasukkan lakukan enkripsi shift dengan key adalah jam saat itu. Untuk melakukan backup setiap 2 jam, menggunakan cronjob, periksa apakah di crontab sudah ada penjadwalan untuk file enkripsi ini, jika belum ada maka tambahkan cronjob untuk file ini jalan tiap 2 jam.
Untuk file dekripsinya, langkahnya sama yaitu mengambil isi teks di file backup yang sudah terenkrip, dan memasukkan per line ke file baru, sebelum dimasukkan di dekrip dengan kebalikan dari enkripnya, key nya diambil dari 2 digit nama file yang merupakan jam.

## Source Code:
## log_encrypt.sh
```bash
#!/bin/bash
currentTime=$(date +%H:%M)
currentDate=$(date +%d:%m:%Y)
filename="${currentTime} ${currentDate}.txt"

inputFile="/var/log/syslog"
outputFile="${filename}"

echo "Sedang Mengenkripsi"

if crontab -l 2>/dev/null | grep -q "$PWD/log_encrypt.sh"; then
    (crontab -l 2>/dev/null | grep -v "$PWD/log_encrypt.sh" ; echo "0 */2 * * * $PWD/log_encrypt.sh") | crontab - >/dev/null 2>&1
else
    (crontab -l 2>/dev/null ; echo "0 */2 * * * $PWD/log_encrypt.sh") | crontab - >/dev/null 2>&1
fi

currentHour=$(date +'%H' | sed 's/^0//')
while read line
do
    output=""
    for ((i=0; i<${#line}; i++)); do
        char=${line:$i:1}
        isUp=0
        if [[ "$char" =~ [a-zA-Z] ]]; then
            desChar=$(printf '%d' "'$char")
            if [[ $desChar -ge 65 && $desChar -le 90 ]]; then
                isUp=1
            fi
            if (( $desChar + currentHour > 90 && $isUp )) || (( $desChar + currentHour > 122 && ! $isUp )); then
                charEnkrip=$(printf \\$(printf '%03o' "$(( $desChar + currentHour - 26 ))"))
                output="${output}${charEnkrip}"
            else
                charEnkrip=$(printf \\$(printf '%03o' "$(( $desChar + currentHour ))"))
                output="${output}${charEnkrip}"
            fi
        else
            output="${output}${char}"
        fi
    done

    echo "${output}" >> "${outputFile}"
done < "${inputFile}"
echo "Selesai"

```
Keterangan:
Membuat file untuk tempat backupnya sesuai format soal, ambil syslog, Lakukan crontab, ambil jam sebagai kunci, lakukan enkripsi

proses enkripsi:
loop dilakukakn dengan mengambil line dari syslog, kemudian ambil per digitnya, cek apakah huruf, cek apakah huruf kapital, lakukan shift sesuai soal, masukkan hasil enkripsi ke file backup

## log_decrypt.sh
```bash
#!/bin/bash

filename="$1"
currentHour=$(echo "${filename:0:2}" | sed 's/^0//')

inputFile="${filename}"
outputFile="decrypted_${filename}"

echo "Sedang mendekripsi"

while read line
do
    output=""
    for ((i=0; i<${#line}; i++)); do
        char=${line:$i:1}
        isUp=0
        if [[ "$char" =~ [a-zA-Z] ]]; then
            desChar=$(printf '%d' "'$char")
            if [[ $desChar -ge 65 && $desChar -le 90 ]]; then
                isUp=1
            fi
            if (( $desChar - currentHour < 65 && $isUp )) || (( $desChar - currentHour < 97 && ! $isUp )); then
                charDekrip=$(printf \\$(printf '%03o' "$(( $desChar - currentHour + 26 ))"))
                output="${output}${charDekrip}"
            else
                charDekrip=$(printf \\$(printf '%03o' "$(( $desChar - currentHour ))"))
                output="${output}${charDekrip}"
            fi
        else
            output="${output}${char}"
        fi
    done

    echo "${output}" >> "${outputFile}"
done < "${inputFile}"
echo "Selesai"

```
Keterangan:
Mengambil input file yang akan di decrypt, ambil key dengan mengambil 2 digit pertama nama file yang merupakan jam, lakukan loop dekripsi.

proses dekripsi:
Sama seperti proses enkripsi, hanya dibalik saja yang awalnya ditambahkan dengan key, ini dikurangkan dengan key nya.

## Test output
Enkripsi
![Enkripsi](https://i.ibb.co/qdWVzVL/buktiEnk.jpg)

Dekripsi
![Dekripsi](https://i.ibb.co/Gp8c0vG/buktiDek.jpg)

## Kendala
Dalam pengambilan jam, hasil dari pengambilan jam terdapat angka 0 diawal (contoh: 08, 09). Program membaca angka yang diawali 0 adalah bilangan oktal, ini menyebabkan error. Sehingga diperlukan untuk menghapus angka 0 jika diawal, dengan sed. 
Untuk membuat cronjob, apabila crontab belum pernah dibuat maka akan muncul output "no crontab for [user]" untuk menghilangkan itu menggunakan /dev/null.
